﻿=================================================
JSON COMPILER v1.0
=================================================

di Tada, Teresa Celina
Maio, Sebastián
Sespede, Braulio
Martinez, Felipe

=================================================

Para ejecutar el compilador es necesario Java SE7 como mínimo.

Para ejecutar el compilador realizar el siguiente comando:
java -jar JSON-Compiler.jar json-a-leer.txt nombre-serializado

El compilador leerá el input de entrada de la carpeta JSONs, y
guardará el JSON compilado en la carpeta JSONs/Compiled.

Para ejecutar los testeos:
java -jar Tests.jar

=================================================
