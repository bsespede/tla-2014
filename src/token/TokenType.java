package token;

public enum TokenType {
	
	// Los distintos tipos de TOKEN con su respectiva gramatica
	BEGINOBJECT("\\{"),
	ENDOBJECT("\\}"),
	BEGINARRAY("\\["),
	ENDARRAY("\\]"),
	COMMA("\\,"),
	COLON("\\:"),
	NULL("null"),
	TRUE("true"),
	FALSE("false"),
	WHITESPACE("[ \t\r\n]+"),
	STRING("\\\"([^\\p{Cntrl}\"\\u005c]|\\\\[bfnrt\"\\\\\\/]|\\\\u[0-9a-f]{4})*\\\""),
	NUMBER("-?(0|[1-9]\\d*)(\\.\\d+)?([eE][+-]?\\d+)?");
	
    public final String pattern;

    private TokenType(String pattern) {
        this.pattern = pattern;
    }
}
