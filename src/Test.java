import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;

import objects.Node;

public class Test implements Serializable{

	private static final long serialVersionUID = -4863626471182031424L;

	public static void main(String[] args) {
		Test testRun = new Test();
		testRun.test();
	}	
	
	public void test(){

		// Test serialization
		String[] arguments = new String[] {"json.txt","testJson1"};
		Main.main(arguments);

		arguments = new String[] {"json2.txt","testJson2"};
		Main.main(arguments);
		
		//Deserializing Process to test created node
		Node node1 = null;
		Node node2 = null;
		try
	      {
	         FileInputStream fileIn = new FileInputStream("JSONs/Compiled/testJson1.json");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         node1 = (Node) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("TestJson1 not found");
	         c.printStackTrace();
	         return;
	      }
		
		try
	      {
	         FileInputStream fileIn = new FileInputStream("JSONs/Compiled/testJson2.json");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         node2 = (Node) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("TestJson2 not found");
	         c.printStackTrace();
	         return;
	      }
				
		//json.txt
		checkSum((node1.getObjectNode()).get("integer").getNumberNode().value(), (node1.getObjectNode()).get("float").getNumberNode().value());
		getShort(node1.getObjectNode().get("name").getStringNode().value());
		checkBool(node1.getObjectNode().get("bool").getBooleanNode().value());
				
		//json2.txt
		searchMaxId(node2.getArrayNode().getNodes());
		testNull(((node2.getArrayNode().get(0).getObjectNode()).get("activityObject").getNullNode().value()));
	}
	
	public void checkSum(float f1, float f2){	
		System.out.println("<TEST SUMA DE NUMERO>");
		float resp = (f1 + f2);	
		System.out.println("checkSum: " + resp);
	}
	
	//Checks class name
	public void searchMaxId(List<Node> list){	
		System.out.println("<TEST ENCONTRAR ID MAXIMO DEL ARRAY>");
		float max = 0;
		for (Node node: list){
			float cur = node.getObjectNode().get("id").getNumberNode().value();
			if (cur>max){
				max = cur;
			}
		}
		System.out.println("Max ID numbers: "+(int) max);	
	}

	//Shortens String
	public void getShort(String value) {
		System.out.println("<TEST ARMAR SUBSTRING DE UN STRING>");
		System.out.println("Name: " + value);
		System.out.println("Shortened name: " + value.substring(0,3));	
	}
	
	//Checks Boolean 
	public void checkBool(boolean b) {
		System.out.println("<TEST TIPOS BOOLEANOS>");
		if(b){
			System.out.println("true");
		}else{
			System.out.println("false");	
		}
	}
	
	//Checks null
	public void testNull(Object test){
		System.out.println("<TEST NULL OBJECTS>");
		if(test == null){
			System.out.println("Es null!");
		}
	}
	
}
