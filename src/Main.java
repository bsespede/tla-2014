import java.util.ArrayList;

import token.Token;
import exception.IllegalGrammarException;
import analyzers.LexicAnalyzer;
import analyzers.SyntacticAnalyzer;
import objects.Node;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

	public static void main(String[] args) {
		
		BufferedReader iObuffer = null;
		String jsonString = new String();
		 
		try {
			
 			String currentLine; 
			iObuffer = new BufferedReader(new FileReader("JSONs/"+args[0]));	
			
			while ((currentLine = iObuffer.readLine()) != null) {				
				if(currentLine != "\n"){
					jsonString += currentLine;
				}			} 
		} catch (IOException e) {
			System.out.println("An error occured while opening the file");
			System.exit(0);
		} finally {
			try {
				if (iObuffer != null)iObuffer.close();
			} catch (IOException ex) {
				System.out.println("An error occured while closing the file");
				System.exit(0);
			}
		}		
		
		LexicAnalyzer lexer = new LexicAnalyzer();
		SyntacticAnalyzer synt = new SyntacticAnalyzer();
		Node node = null;
		
		try {
			
			long elapsedTime = System.nanoTime();
			ArrayList<Token> tlist= lexer.analyze(jsonString);
			
			try{
				node = synt.analize(tlist);
				if(node == null){
					System.out.print("Invalid json syntax.");
					System.exit(0);
				}else{				
					System.out.println("Valid json.");					
				}
			}catch(Exception e){
				System.out.println(e);
			}
			
			elapsedTime = (System.nanoTime() - elapsedTime)/1000000;
			System.out.println("Elapsed time: "+ elapsedTime +"ms.");			
		} catch (IllegalGrammarException e) {
			e.printStackTrace();
			System.exit(0);
		}	
		
		try
	    {
	       FileOutputStream fileOut = new FileOutputStream("JSONs/Compiled/"+args[1]+".json");
	       ObjectOutputStream out = new ObjectOutputStream(fileOut);
	       out.writeObject(node);
	       out.close();
	       fileOut.close();
	       System.out.println("Serialized data is saved: "+args[1]+".json");
	    }catch(IOException i)
	    {
	        i.printStackTrace();
	        System.exit(0);
	    }
	}
}
