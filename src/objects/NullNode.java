package objects;

import java.io.Serializable;


public class NullNode implements NodeType, Serializable{
	
	private static final long serialVersionUID = 5508245335950623714L;

	public Object value() {
		return null;
	}
	
	public NullNode get(String s){
		return this;
	}
	
	public String toString(){
		return String.valueOf("null");
	}
}
