package objects;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ArrayNode implements NodeType, Serializable{
	
	private static final long serialVersionUID = 1886949417189013299L;
	
	List<Node> nodes = new LinkedList<Node>();
	
	public Node get(int number){
		return nodes.get(number);
	}

	public void add(Node newNode){
		nodes.add(newNode);
	}
	
	public String toString(){
		String str = new String();
		for (NodeType n : nodes) {
			str += n.toString();
		}
		return str;
	}
	
	public List<Node> getNodes(){
		return nodes;
	}
}
