package objects;

import java.io.Serializable;


public class Node implements NodeType, Serializable{

	private static final long serialVersionUID = 7913909990873524624L;
	
	ArrayNode arrayNode;
	ObjectNode objectNode;
	BooleanNode literalNode;
	NumberNode numberNode;
	StringNode stringNode;
	NullNode nullNode;
	
	public String toString(){
		String str = new String();
		if(arrayNode != null){
			str += arrayNode.toString();
		}
		else if(objectNode != null){
			str += objectNode.toString();
		}
		else if(literalNode != null){
			str += literalNode.toString();
		}
		else if(numberNode != null){
			str += numberNode.toString();
		}
		else if(stringNode != null){
			str += stringNode.toString();
		}
		else if(nullNode != null){
			str += nullNode.toString();
		}
		return str;
		
	}
	

	
	
	public NullNode getNullNode() {
		return nullNode;
	}
	
	public void setNullNode(NullNode nullNode) {
		this.nullNode = nullNode;
	}
	
	public ArrayNode getArrayNode() {
		return arrayNode;
	}
	
	public void setArrayNode(ArrayNode arrayNode) {
		this.arrayNode = arrayNode;
	}
	
	public ObjectNode getObjectNode() {
		return objectNode;
	}
	
	public void setObjectNode(ObjectNode objectNode) {
		this.objectNode = objectNode;
	}
	
	public BooleanNode getBooleanNode() {
		return literalNode;
	}
	
	public void setBooleanNode(BooleanNode literalNode) {
		this.literalNode = literalNode;
	}
	
	public NumberNode getNumberNode() {
		return numberNode;
	}
	
	public void setNumberNode(NumberNode numberNode) {
		this.numberNode = numberNode;
	}
	
	public StringNode getStringNode() {
		return stringNode;
	}
	
	public void setStringNode(StringNode stringNode) {
		this.stringNode = stringNode;
	}
}
