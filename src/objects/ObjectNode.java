package objects;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ObjectNode implements NodeType, Serializable{

	private static final long serialVersionUID = 1634086264634883557L;
	
	Map<String, Node> objects = new HashMap<String, Node>();
	
	public Node get(String key){
		return objects.get(key);
	}
	
	public void add(String key, Node value){
		objects.put(key.substring(1, key.length() -1), value);
	}

	public String toString(){
		String str = new String();
		for(Entry<String, Node> s : objects.entrySet()){
			str += (s.getKey().toString() + " : " + s.getValue().toString() + "\n");				
		}
		return str;
	}
}
