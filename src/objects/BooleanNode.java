package objects;

import java.io.Serializable;


public class BooleanNode implements NodeType, Serializable{

	private static final long serialVersionUID = -3169294469003055677L;
	
	boolean value;
	
	public BooleanNode(boolean value){
		this.value = value;
	}	
	
	public boolean value() {
		return value;
	}	
	
	public String toString(){
		return String.valueOf(value);
	}
}
