package objects;

import java.io.Serializable;

public class NumberNode implements NodeType, Serializable{

	private static final long serialVersionUID = -8158055266333841383L;
	
	float value;
	
	public NumberNode(float value){
		this.value = value;
	}	
	
	public float value() {
		return value;
	}	
	
	public String toString(){
		return String.valueOf(value);
	}
}
