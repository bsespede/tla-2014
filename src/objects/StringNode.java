package objects;

import java.io.Serializable;

public class StringNode implements NodeType, Serializable {

	private static final long serialVersionUID = -7763204088434281982L;
	
	String value;
	
	public StringNode(String value){
		this.value = value.substring(1, value.length() -1);
	}	
	
	public String value() {
		return value;
	}	
	
	public String toString(){
		return value;
	}
}
