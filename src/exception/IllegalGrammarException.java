package exception;

public class IllegalGrammarException extends Exception{

	private static final long serialVersionUID = 5345928342197349819L;

	public IllegalGrammarException(String string) {
		super(string);
	}

}
