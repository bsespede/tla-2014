package analyzers;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import token.*;
import exception.*;

public class LexicAnalyzer {	
	
    public ArrayList<Token> analyze(String input) throws IllegalGrammarException {
    	
        // La lista de tokens
        ArrayList<Token> tokens = new ArrayList<Token>();

        // Junto todos los patrones para poder agarrarlos por grupo
        StringBuffer tokenPatternsBuffer = new StringBuffer();
        for (TokenType tokenType : TokenType.values()){
            tokenPatternsBuffer.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));
        }
        
        // Compilo la regex y creo el matcher
        Pattern tokenPatterns = Pattern.compile(new String(tokenPatternsBuffer.substring(1)));
        boolean finished = false;
        int lastMatch = 0;
        Matcher matcher = tokenPatterns.matcher(input);
        
        while (!finished) {
        	// Si algun patron machea desde el principio
        	if(matcher.lookingAt()){
        		lastMatch = matcher.end();
        		// Me fijo cual fue el que macheo y lo agrego a la lista de tokens menos los whitespaces que los ignoro
        		for (TokenType token: TokenType.values()){        			
        			if(matcher.group(token.name())!= null){
        				if(token != TokenType.WHITESPACE){
        					tokens.add(new Token(token, matcher.group(token.name())));
        				}
        				matcher.region(matcher.end(), matcher.regionEnd());
        				break;
        			}
        		}
        	}else{
        		if(lastMatch == input.length()){
        			finished = true;
        		}else{
        			throw new IllegalGrammarException("Does not belong to grammar:"+input.substring(lastMatch));
        		}        		
        	}
        }        
        return tokens;
    }
}