package analyzers;

import java.util.ArrayList;

import objects.ArrayNode;
import objects.BooleanNode;
import objects.Node;
import objects.NullNode;
import objects.NumberNode;
import objects.ObjectNode;
import objects.StringNode;
import token.Token;
import token.TokenType;
import exception.IllegalGrammarException;

public class SyntacticAnalyzer {
	static int tindex;
	
	public Node analize(ArrayList<Token> tlist) throws IllegalGrammarException{
		tindex=0;
		Node first = I(tlist);
		
		if(first != null){
			if(tindex==tlist.size()){
				return first;
			}
			return null;
		}
		return null;
	}
	
	private TokenType getActual(ArrayList<Token> tlist, int tindex){
		return tlist.get(tindex).type;
	}
	
	private String getActualValue(ArrayList<Token> tlist, int tindex){
		return tlist.get(tindex).value;
	}	
	
	
	private Node I(ArrayList<Token> tlist) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual = getActual(tlist,tindex);
		Node newNode = new Node();
		if(actual == TokenType.BEGINOBJECT){
			ObjectNode objNode = O(tlist);
			if (objNode != null){
				newNode.setObjectNode(objNode);
			}
			return newNode;
		}else if(actual == TokenType.BEGINARRAY){
			ArrayNode arrNode = A(tlist);
			if (arrNode != null){
				newNode.setArrayNode(arrNode);
			}
			return newNode;
		}else{
			return null;
		}		
	}
	
	private ObjectNode O(ArrayList<Token> tlist) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		if(getActual(tlist,tindex) == TokenType.BEGINOBJECT){
			tindex++;
			ObjectNode objNode = Oprima(tlist, new ObjectNode());
			if(tlist.size() <= tindex){
				throw new IllegalGrammarException("Syntax error has been found, Illegal json");
			}
			if(objNode != null && getActual(tlist,tindex) == TokenType.ENDOBJECT){
				tindex++;
				return objNode;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private ObjectNode Oprima(ArrayList<Token> tlist, ObjectNode objNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual= getActual(tlist, tindex);
		if(actual==TokenType.STRING){
			return M(tlist, objNode);
		}else if(actual == TokenType.ENDOBJECT){
			return objNode;
		}else{
			return null;
		}
	}
	
	private ObjectNode M(ArrayList<Token> tlist, ObjectNode objNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		if(getActual(tlist, tindex)==TokenType.STRING && P(tlist, objNode)!=null){
			return Mprima(tlist, objNode);
		}else {
			return null;
		}
	}
	
	private ObjectNode Mprima(ArrayList<Token> tlist, ObjectNode objNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual= getActual(tlist,tindex);
		if(actual==TokenType.COMMA){
			tindex++;		
			return M(tlist, objNode);
		}else if(actual==TokenType.ENDOBJECT){
			return objNode;
		}else{
			return null;
		}
	}
	private ObjectNode P(ArrayList<Token> tlist, ObjectNode objNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		if(getActual(tlist, tindex)==TokenType.STRING){
			String key= getActualValue(tlist, tindex);
			tindex++;
			if(tlist.size() <= tindex){
				throw new IllegalGrammarException("Syntax error has been found, Illegal json");
			}
			
			if(getActual(tlist, tindex)==TokenType.COLON){
				tindex++;
				Node valueNode = V(tlist);
				if(valueNode != null){
						objNode.add(key, valueNode);
				}
				return objNode;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private ArrayNode A(ArrayList<Token> tlist) throws IllegalGrammarException{		
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		if(getActual(tlist,tindex) == TokenType.BEGINARRAY){
			tindex++;
			ArrayNode arrNode = Aprima(tlist, new ArrayNode());
			if(tlist.size() <= tindex){
				throw new IllegalGrammarException("Syntax error has been found, Illegal json");
			}
			if(arrNode!=null && getActual(tlist,tindex) == TokenType.ENDARRAY){
				tindex++;
				return arrNode;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private ArrayNode Aprima(ArrayList<Token> tlist, ArrayNode arrayNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual= getActual(tlist, tindex);
		if(actual==TokenType.BEGINARRAY || actual==TokenType.BEGINOBJECT || actual==TokenType.TRUE || actual==TokenType.FALSE || actual==TokenType.NULL || actual==TokenType.STRING || actual==TokenType.NUMBER){
			ArrayNode newArrNode = E(tlist, arrayNode);
			if(newArrNode != null){
				return newArrNode;
			}else if(actual==TokenType.ENDARRAY){
				return arrayNode;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private ArrayNode E(ArrayList<Token> tlist, ArrayNode arrayNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual= getActual(tlist, tindex);
		if(actual==TokenType.BEGINARRAY || actual==TokenType.BEGINOBJECT || actual==TokenType.TRUE || actual==TokenType.FALSE || actual==TokenType.NULL || actual==TokenType.STRING || actual==TokenType.NUMBER){
			Node valueNode = V(tlist);
			if(valueNode != null){
				arrayNode.add(valueNode);
				return Eprima(tlist, arrayNode);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	private ArrayNode Eprima(ArrayList<Token> tlist, ArrayNode arrayNode) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual = getActual(tlist,tindex);
		if(actual == TokenType.COMMA){
			tindex++;
			return E(tlist, arrayNode);
		}else if (actual == TokenType.ENDARRAY){
			return arrayNode;
		}else{
			return null;
		}
	}
	
	private Node V(ArrayList<Token> tlist) throws IllegalGrammarException{
		if(tlist.size() <= tindex){
			throw new IllegalGrammarException("Syntax error has been found, Illegal json");
		}
		TokenType actual= getActual(tlist, tindex);
		String actualVal = getActualValue(tlist, tindex);
		Node newNode = new Node();
		if(actual==TokenType.TRUE){
			tindex++;			
			newNode.setBooleanNode(new BooleanNode(true));
			return newNode;
		}else if(actual==TokenType.FALSE){
			tindex++;
			newNode.setBooleanNode(new BooleanNode(false));
			return newNode;
		}else if(actual==TokenType.NULL){
			tindex++;
			newNode.setNullNode(new NullNode());
			return newNode;
		}else if(actual==TokenType.STRING){
			tindex++;
			newNode.setStringNode(new StringNode(actualVal));
			return newNode;
		}else if(actual==TokenType.NUMBER){
			tindex++;
			newNode.setNumberNode(new NumberNode(new Float(actualVal)));
			return newNode;
		}else if(actual==TokenType.BEGINARRAY){		
			ArrayNode arrNode = A(tlist);
			if (arrNode != null){
				newNode.setArrayNode(arrNode);
				return newNode;
			}else{
				return null;
			}
		}else if(actual==TokenType.BEGINOBJECT){		
			ObjectNode objNode = O(tlist);
			if (objNode != null){
				newNode.setObjectNode(objNode);
				return newNode;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
}
