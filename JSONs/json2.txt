[
  {
    "id": 0,
    "guid": "3f366165-52c9-4c30-adcc-5e770e31c922",
    "activityObject": null,
    "balance": "$2,532.53",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "green",
    "name": "Sandoval Mathews",
    "gender": "male",
    "company": "JAMNATION",
    "email": "sandovalmathews@jamnation.com",
    "phone": "+1 (918) 586-3848",
    "address": "182 Doone Court, Eagletown, North Dakota, 5802",
    "about": "Cupidatat sunt elit nostrud mollit nulla in dolor. Ex sunt et sint anim dolor in veniam laboris incididunt reprehenderit officia culpa adipisicing voluptate. Labore voluptate duis pariatur laborum qui nisi.\r\n",
    "registered": "2014-03-19T02:53:52 +03:00",
    "latitude": -57.109365,
    "longitude": -165.562852,
    "tags": [
      "proident",
      "adipisicing",
      "eu",
      "pariatur",
      "pariatur",
      "incididunt",
      "ea"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Deena Ingram"
      },
      {
        "id": 1,
        "name": "Mcbride Zimmerman"
      },
      {
        "id": 2,
        "name": "Petty Wallace"
      }
    ],
    "greeting": "Hello, Sandoval Mathews! You have 1 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "id": 1,
    "guid": "fafe6513-31db-4ede-b3cf-c808a0fc19e5",
    "isActive": false,
    "balance": "$1,051.45",
    "picture": "http://placehold.it/32x32",
    "age": 28,
    "eyeColor": "green",
    "name": "Minnie Miles",
    "gender": "female",
    "company": "MAGNAFONE",
    "email": "minniemiles@magnafone.com",
    "phone": "+1 (871) 458-2332",
    "address": "923 Eldert Street, Goochland, New York, 5596",
    "about": "Consequat elit esse minim occaecat laboris consectetur in ut deserunt commodo et Lorem. Sunt aliqua mollit aliquip laboris ex elit. Consequat commodo esse nulla consequat veniam amet deserunt quis dolor sint sit proident. Sint in do quis ut aliqua exercitation occaecat nisi culpa. Nisi enim sint aliquip magna veniam ex.\r\n",
    "registered": "2014-01-15T00:24:46 +03:00",
    "latitude": 22.899873,
    "longitude": -152.253178,
    "tags": [
      "in",
      "dolore",
      "deserunt",
      "voluptate",
      "qui",
      "adipisicing",
      "ipsum"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Elliott Calhoun"
      },
      {
        "id": 1,
        "name": "Knapp Austin"
      },
      {
        "id": 2,
        "name": "Suzette Kent"
      }
    ],
    "greeting": "Hello, Minnie Miles! You have 10 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "id": 2,
    "guid": "3ca58d07-0f0c-4758-98c9-d52dda41022e",
    "isActive": false,
    "balance": "$2,550.68",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "green",
    "name": "Elena Owen",
    "gender": "female",
    "company": "VERTIDE",
    "email": "elenaowen@vertide.com",
    "phone": "+1 (991) 429-3850",
    "address": "642 Clove Road, Finzel, California, 2872",
    "about": "Exercitation dolor ullamco esse dolor. Ut sunt qui ex aliqua deserunt pariatur quis. Officia est Lorem quis culpa ullamco incididunt cupidatat magna. Aliquip nulla enim aliquip nisi cillum qui duis esse aliqua mollit anim. Quis non sint dolor culpa est aliqua minim consequat dolore sint Lorem nulla ipsum. Non proident esse non consectetur ipsum quis magna. Pariatur quis culpa sunt Lorem quis commodo.\r\n",
    "registered": "2014-03-27T05:11:33 +03:00",
    "latitude": -36.729581,
    "longitude": 153.416676,
    "tags": [
      "labore",
      "ea",
      "mollit",
      "do",
      "exercitation",
      "ut",
      "sunt"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Melendez Gillespie"
      },
      {
        "id": 1,
        "name": "Gail Clayton"
      },
      {
        "id": 2,
        "name": "Sears Hale"
      }
    ],
    "greeting": "Hello, Elena Owen! You have 7 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "id": 3,
    "guid": "5bccf03f-0b0b-4cfc-a188-6ee6a4b5b52d",
    "isActive": true,
    "balance": "$3,664.15",
    "picture": "http://placehold.it/32x32",
    "age": 30,
    "eyeColor": "blue",
    "name": "Woodard Duncan",
    "gender": "male",
    "company": "CODACT",
    "email": "woodardduncan@codact.com",
    "phone": "+1 (928) 506-3698",
    "address": "517 Leonard Street, Robbins, Virginia, 2296",
    "about": "Tempor sit aute laborum tempor nostrud aliqua occaecat irure pariatur. Eiusmod non culpa nisi magna exercitation id adipisicing. Nisi nisi do adipisicing irure quis sunt. Minim ullamco eiusmod exercitation commodo eu occaecat voluptate ea nisi nostrud officia. Id esse irure irure sint officia amet. Laborum cillum ex dolore laborum sint cupidatat laboris id cillum. Do eu nulla cupidatat eu elit.\r\n",
    "registered": "2014-05-16T04:28:05 +03:00",
    "latitude": 43.026784,
    "longitude": -2.222482,
    "tags": [
      "pariatur",
      "quis",
      "ut",
      "elit",
      "irure",
      "sint",
      "amet"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Dillon Ware"
      },
      {
        "id": 1,
        "name": "Yolanda Crosby"
      },
      {
        "id": 2,
        "name": "Garrison Kane"
      }
    ],
    "greeting": "Hello, Woodard Duncan! You have 6 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "id": 4,
    "guid": "d83c1b42-c304-422c-ae3f-af06ad3ef173",
    "isActive": true,
    "balance": "$2,755.82",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "brown",
    "name": "Romero Bradley",
    "gender": "male",
    "company": "MEMORA",
    "email": "romerobradley@memora.com",
    "phone": "+1 (969) 414-3840",
    "address": "907 Banner Avenue, Kempton, Vermont, 3176",
    "about": "Minim ipsum ex occaecat laboris. Elit et cillum laboris duis consectetur consequat incididunt cillum deserunt in. Commodo minim qui exercitation officia excepteur amet pariatur labore non minim dolore exercitation laboris. Laboris aute laborum officia irure duis ad sint exercitation ullamco sit exercitation.\r\n",
    "registered": "2014-04-28T06:27:48 +03:00",
    "latitude": 19.572422,
    "longitude": 31.690614,
    "tags": [
      "cupidatat",
      "incididunt",
      "exercitation",
      "laborum",
      "laboris",
      "reprehenderit",
      "et"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Potts Mccormick"
      },
      {
        "id": 1,
        "name": "Clay Burks"
      },
      {
        "id": 2,
        "name": "Rosario Gardner"
      }
    ],
    "greeting": "Hello, Romero Bradley! You have 6 unread messages.",
    "favoriteFruit": "strawberry"
  }
]